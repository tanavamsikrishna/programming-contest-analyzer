"""This is blah blah blah
"""
import json
import os
import requests
import numpy
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

contest_id = 1114
contest_data = {}


def fetch_submission_statuses():
    page_size = 1000
    url = 'http://codeforces.com/api/contest.status'
    has_next = True
    page_start = 1
    while has_next:
        print("Current page start is {0}".format(page_start))
        args = {
            'contestId': contest_id,
            'from': page_start,
            'count': page_size
        }
        response = requests.request('GET', url, params=args).json()
        if response['status'] != 'OK':
            print("Response not ok: {0}".format(args))
        else:
            result = response['result']
            if len(result) != page_size:
                has_next = False
            result = list(
                filter(
                    lambda r: r['author']['participantType'] == 'CONTESTANT',
                    result))
            with open('data-{0}.json'.format(page_start), 'w') as f:
                f.write(json.dumps(result))
            page_start += page_size


def analyze():
    analysis = {}
    for file in os.listdir('./raw'):
        with open('./raw/{}'.format(file)) as f:
            submissions = json.loads(f.readline())
            for submission in submissions:
                plang = submission['programmingLanguage']
                verdict = submission['verdict']
                if plang not in analysis.keys():
                    analysis[plang] = {}
                if verdict not in analysis[plang].keys():
                    analysis[plang][verdict] = 0
                analysis[plang][verdict] += 1
    with open('viewable-output.json', 'w') as f:
        f.write(json.dumps(analysis))


def all_statuses():
    with open('viewable-output.json') as f:
        pls_data = json.loads(f.readline())
    statuses = set()
    for _, pl_data in pls_data.items():
        for status in pl_data.keys():
            statuses.add(status)
    print(statuses)


def total_submissions():
    with open('viewable-output.json') as f:
        data = json.loads(f.readline())
    total_submissions = 0
    for _, pl_data in data.items():
        for _, number in pl_data.items():
            total_submissions += number
    print(total_submissions)


def plot():
    with open('viewable-output.json') as f:
        pls_data = json.loads(f.readline())
    pl_names = [pl_name for pl_name in pls_data]
    statuses = [
        'TIME_LIMIT_EXCEEDED', 'IDLENESS_LIMIT_EXCEEDED', 'RUNTIME_ERROR',
        'MEMORY_LIMIT_EXCEEDED', 'WRONG_ANSWER', 'CHALLENGED', 'SKIPPED',
        'COMPILATION_ERROR', 'OK'
    ]
    status_data = {}

    def num_occu(status):
        num_occus = []
        for _, pl_data in pls_data.items():
            if status in pl_data.keys():
                num_occus.append(pl_data[status])
            else:
                num_occus.append(0)
        return num_occus

    for status in statuses:
        status_data[status] = num_occu(status)

    ind = numpy.arange(len(pl_names))
    width = 0.1
    fig: Figure
    fig, a_x = plt.subplots()
    fig.subplots_adjust(bottom=0.2)

    for i in range(len(statuses)):
        a_x.bar(
            ind - (width * (4.5 - i)),
            status_data[statuses[i]],
            width,
            label=statuses[i])

    a_x.set_xticks(ind)
    a_x.set_xticklabels(tuple(pl_names), rotation=90, fontsize=7)
    a_x.legend()
    plt.savefig('all.png', dpi=600)


if __name__ == "__main__":
    total_submissions()
