# Analysis of programming language preference and their affect on submissions
> How does the choice of programming language affect contestant performance in programming contests?

Conclusions are drawn from a programming contest organized by codeforces at http://codeforces.com/contest/1114

The 4 branches in this repo i.e. `all, number_of_submissions, oknotok, success_percentage` deal with 4 different metrics. 

Output of all those branches can be found in [downloads section](https://bitbucket.org/tanavamsikrishna/programming-contest-analyzer/downloads/) in `png` format. The output is inspected and conclusions are drawn in this [blog post](http://www.vamsitalupula.com/posts/programming-contest-analysis)

